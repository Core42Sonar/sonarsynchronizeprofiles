﻿$minikubeip =& minikube ip 
$user = 'admin'
$pass = 'admin'

$pair = "$($user):$($pass)"

$encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
$basicAuthValue = "Basic $encodedCreds"
$filePath = ".\HTML.xml"
$headers = @{
    Authorization = $basicAuthValue     
}

$uri1str="http://"
$uriSetStr=":31000/sonar/api/qualityprofiles/set_default"
$uriSetDefault="$uri1str$minikubeip$uriSetStr"
$setParams =@{language='web';qualityProfile="Core42 way"}

# установить правила по дефолту, работает
Invoke-WebRequest -Uri $uriSetDefault -UseBasicParsing -Method POST -Headers $headers -Body $setParams 