﻿$pullPath=$env:temp+"\SonarRules"

$gitExist=(get-command git.exe).Path
$res = $gitExist.Contains('git.exe') 
if ($res){    
Remove-Item $pullPath -Recurse -Force | Remove-Item
git clone https://gitlab.com/Core42Sonar/sonarsynchronizeprofiles $pullPath
$minikubeip =& minikube ip 

$uri1str="http://"
$uriPushStr=":31000/sonar/api/qualityprofiles/restore"
$uriPushRules="$uri1str$minikubeip$uriPushStr"

cmd /c curl -v POST -u admin:admin $uriPushRules --form backup=@$pullPath"\HTML.xml"

&"$pullPath\SetDefaultRules.ps1"

Remove-Item –path $pullPath -Recurse -Force
}
else{

Write-Host "Git not found, download git https://git-scm.com/"
Start-Sleep -s 15
}



